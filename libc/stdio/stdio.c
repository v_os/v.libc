/*
Modified BSD License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modified sections of source code used in redistributions covered under
   these licensing terms must be provided freely and free of charge in source
   code form to any party which requests the modifications. Only the modified
   sections need to be made available if the redistribution is included in a
   larger body of work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

extern int write(int fd, const char* buf, int len);

int putchar(int ic)
{
    char c = ic & 0xFF;
    return write(1, &c, 1);
}

int puts(const char* string)
{
    int err = write(1, string, strlen(string));
    char nl = '\n';
    err |= write(1, &nl, 1);
    return err;
}

/* print a string of length */
int print(const char* buf, unsigned int len)
{
    return write(1, &buf, len);
}

int printn(unsigned long n, int base, int ucase, int fbuf)
{
    char buf[16];
    char* p = &buf[16 - 1];
    static const char digs[] = "0123456789abcdefghijklmnopqrstuvwxyz";
    static const char udigs[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    do
    {
        if (ucase)
        {
            *p-- = udigs[n % base];
        }
        else
        {
            *p-- = digs[n % base];
        }
        n /= base;
    } while (n != 0);

    while (++p != &buf[16])
    {
        if (putchar(*p) == -1)
        {
            return -1;
        }
    }

    return 0;
}

#include <stdarg.h>

/* printf for kernel debug prints */
int printf(const char* restrict format, ...)
{
    va_list parameters;
    va_start(parameters, format);

    int written = 0;

    while (*format != '\0')
    {
        unsigned int maxrem = 0x7FFFFFFF - written;

        /* check and print plain characters */
        if (format[0] != '%' || format[1] == '%')
        {
            if (format[0] == '%')
            {
                format++;
            }
            unsigned int amount = 1;
            while (format[amount] && format[amount] != '%')
            {
                amount++;
            }
            if (maxrem < amount)
            {
                return -1;
            }
            if (print(format, amount) < 0)
            {
                return -1;
            }
            format += amount;
            written += amount;
            continue;
        }

        const char* format_begun_at = format++;

        /* character */
        if (*format == 'c')
        {
            format++;
            char c = (char)va_arg(parameters, int);
            if (!maxrem)
            {
                return -1;
            }
            if (print(&c, sizeof(c)) < 0)  //?????
            {
                return -1;
            }
            written++;
        }
        /* string */
        else if (*format == 's')
        {
            format++;
            const char* str = va_arg(parameters, const char*);
            unsigned int len = strlen(str);
            if (maxrem < len)
            {
                return -1;
            }
            if (print(str, len))
            {
                return -1;
            }
            written += len;
        }
        /* decimal integer */
        else if (*format == 'd' || *format == 'i' || *format == 'u')
        {
            format++;
            int n = va_arg(parameters, int);
            unsigned int len = n % 10;
            if (maxrem < len)
            {
                return -1;
            }
            if (printn(n, 10, 0, maxrem))
            {
                return -1;
            }
            written += len;
        }
        /* decimal long integer */
        else if (*format == 'l')
        {
            format++;
            long n = va_arg(parameters, long);
            unsigned int len = n % 10;
            if (maxrem < len)
            {
                return -1;
            }
            if (printn(n, 10, 0, maxrem))
            {
                return -1;
            }
            written += len;
        }
        /* hex integer */
        else if (*format == 'x' || *format == 'X')
        {
            format++;
            int n = va_arg(parameters, int);
            unsigned int len = n % 16;
            if (maxrem < len)
            {
                return -1;
            }
            if (printn(n, 16, ('x' - *format), maxrem))
            {
                return -1;
            }
            written += len;
        }
        /* octal integer */
        else if (*format == 'o' || *format == 'O')
        {
            format++;
            int n = va_arg(parameters, int);
            unsigned int len = n % 8;
            if (maxrem < len)
            {
                return -1;
            }
            if (printn(n, 8, 0, maxrem))
            {
                return -1;
            }
            written += len;
        }
        /* don't know so print like normal characters */
        else
        {
            format = format_begun_at;
            unsigned int len = strlen(format);
            if (maxrem < len)
            {
                return -1;
            }
            if (print(format, len) < 0)
            {
                return -1;
            }
            written += len;
            format += len;
        }
    }

    va_end(parameters);

    return written;
}
