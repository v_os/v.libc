/*
Modified BSD License

Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modified sections of source code used in redistributions covered under
   these licensing terms must be provided freely and free of charge in source
   code form to any party which requests the modifications. Only the modified
   sections need to be made available if the redistribution is included in a
   larger body of work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

extern int main();
extern void _exit(int code);

__attribute__((weak)) void _start_inits(void)
{
    /*
       This will be overridden by VCC if required
    */
}

#if defined(__ARM__) && defined(__arm__) && defined(__CORTEX_M)

/* exec entrance routine */
__attribute__((noinline)) int _start(void)
{
    register int t1 __asm("r4");
    register int t2 __asm("r5");
    register int t3 __asm("r6");
    int a1 = t1;
    int a2 = t2;
    int a3 = t3;
    _start_inits();
    _exit(main(a1, a2, a3));
    return -1;
}

#else

__attribute__((noinline)) int _start(void)
{
    _exit(-1);
    return -1;
}

#endif
