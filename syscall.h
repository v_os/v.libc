// /*
// Modified BSD License

// Copyright (c) 2019-2023, Chloe Lunn <chloetlunn@gmail.com>
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.

// 4. Any modified sections of source code used in redistributions covered under
//    these licensing terms must be provided freely and free of charge in source
//    code form to any party which requests the modifications. Only the modified
//    sections need to be made available if the redistribution is included in a
//    larger body of work.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//  */

// #if !H_SYSCALL
// #define H_SYSCALL 1

// extern int syscall(register int sysnum, register int a, register int b, register int c, int d, int e, int f);
// extern void exit(int r);
// extern int fork(void);
// extern int wait(pid_t wpid, int* status, int options, struct rusage* rusage);
// extern int sleep(int sec);
// extern int kill(pid_t pid, int sig);
// extern int killpg(pid_t pgrp, int sig);
// extern int execve(const char* path, const char* argv[], const char* envp[]);
// extern int reboot(int how);
// extern void yield(void);
// extern int setpriority(int which, id_t who, int nice);
// extern int getpriority(int which, id_t who);
// extern int setsched(struct sched*);
// extern int getsched(struct sched*);
// extern int kerrno(int*);
// extern int kputc(int);
// extern int kgetc();
// extern int open(const char* path, int flag, int mode);
// extern int read(int fd, char* buf, size_t count);
// extern int write(int fd, char* buf, size_t count);
// extern int seek(int fd, off64_t offset, int where);
// extern int close(int fd);
// extern int umask(int mask);
// extern int creat(char* path, mode_t mode);
// extern int mkdir(const char* pathname, mode_t mode);
// extern int mknod(const char* path, mode_t mode, dev_t dev);
// extern int mkfifo(const char* pathname, mode_t mode);
// extern int mksipc(char* path, int mode);
// extern int dup3(int oldfd, int newfd, int flags);
// extern int link(char* src, char* dst);
// extern int unlink(char* dst);
// extern int symlink(const char* target, const char* linkpath);
// extern int readlink(const char* pathname, char* buf, size_t bufsiz);
// extern int flock(int fd, int operation);
// extern int access(char* path, mode_t mode);
// extern int getacl(char* path, struct acl*, int nacl);
// extern int setacl(char* path, struct acl*, int nacl);
// extern int getxattr(char* path, char*, int len);
// extern int setxattr(char* path, char*, int len);
// extern int fpath(int fd, char* path);
// extern int chmod(const char* path, mode_t mode);
// extern int chown(const char* path, uid_t uid, gid_t group);
// extern int chflags(char* path, int flags);
// extern int utimes(const char* filename, const struct timeval tv[2]);
// extern int stat(const char* pathname, struct stat* statbuf);
// extern int fsync(int fd);
// extern int ready(int fd, int flags);
// extern int ioctl(int fd, int cmd, void* arg, int mode);
// extern int truncate(const char* path, off_t length);
// extern void sync(void);
// extern int mount(char* dev_path, char* mount_path, char* type, int flag, void* data);
// extern int umount(char* mount_path, int flag);
// extern int statfs(const char* path, struct statfs* buf);
// extern int chdir(char* path);
// extern int chroot(const char* path);
// extern int getdents(int fd, void* dirp, size_t count);
// extern int pipe2(int* pipefd, int flags);
// extern int sipcall(int fd, int call, int* args);
// extern int regcall(int fd, int call, const sipc_t* fn);
// extern int sigsetmask(int mask);
// extern sig_t signal(int signum, sig_t handler);
// extern int getpid(void);
// extern int getppid(void);
// extern int setgid(gid_t gid);
// extern int getgid(void);
// extern int setegid(gid_t gid);
// extern int getegid(void);
// extern int setuid(uid_t);
// extern int getuid(void);
// extern int seteuid(uid_t);
// extern int geteuid(void);
// extern int setpgrp(pid_t pid, pid_t pgrp);
// extern int getpgrp(pid_t pid);
// extern int setsid(void);
// extern int getsid(void);
// extern int getgroups(int gidsetsize, gid_t* grouplist);
// extern int setgroups(int gidsetsize, gid_t* grouplist);
// extern int uname(struct utsname* buf);
// extern int brk(caddr_t addr);
// extern int sbrk(int incr);
// extern int sstk(int incr);
// extern gettimeofday;
// extern settimeofday;
// extern int socket(int domain, int type, int protocol);
// extern int socketpair(int domain, int type, int protocol, int sv[2]);
// extern int bind(int sockfd, const struct sockaddr* addr, socklen_t addrlen);
// extern int connect(int sockfd, const struct sockaddr* addr, socklen_t addrlen);
// extern int listen(int sockfd, int backlog);
// extern int accept(int s, struct sockaddr* addr, socklen_t* addrlen);
// extern int shutdown(int sockfd, int how);
// extern int getpeername(int s, struct sockaddr* name, socklen_t* namelen);
// extern int getsockname(int s, struct sockaddr* name, socklen_t* namelen);
// extern int setsockopt(int sockfd, int level, int optname, void* optval, socklen_t optlen);
// extern int getsockopt(int sockfd, int level, int optname, void* optval, socklen_t* optlen);
// extern int send(int sockfd, const void* buf, size_t len, int flags);
// extern int sendto(int sockfd, const void* buf, size_t len, int flags, const struct sockaddr* dest_addr, socklen_t addrlen);
// extern int recv(int sockfd, void* buf, size_t len, int flags);
// extern int recvfrom(int s, void* buf, size_t len, int flags, struct sockaddr* from, socklen_t* fromlen);

// #endif
