# VCC

VCC is a front-end wrapper for the arm-none-eabi-gcc version of GCC that automatically sets up the required arguments and options for compiling programs targeting V.OS


In short it adds the following arguments to the program:

-mcpu=cortex-m4 -mthumb -nostdlib -nolibc -ffreestanding -static -fPIE -D__SAMD51__ echo.c -o echo -z max-page-size=4096 -Ttext=0


It will also need to handle unpacking global pointers that are pre-assigned to be the pointer, plus the value, and then create a _start_init that will link it up
