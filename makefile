

# cc -nostdlib -ffreestanding  -D__v_os_libc -D__v_os__ -D__V_OS__ ./libc/string/strcmp.c -c
# ar

-mcpu=cortex-m4 -mthumb -nostdlib -nolibc -ffreestanding -static -fPIE -D__SAMD51__ echo.c -o echo -z max-page-size=4096 -Ttext=0


CC = arm-none-eabi-gcc
ASM = arm-none-eabi-as
AR = arm-none-eabi-ar
STRIP = arm-none-eabi-strip
LINKER = arm-none-eabi-ld

CFLAGS = -mcpu=cortex-m4 -mthumb -fPIE -DPIE -ffunction-sections -fdata-sections -fno-strict-aliasing -fno-builtin -Wall -g -Os \
         -nostdlib -nolibc -ffreestanding -static -fPIE -D__SAMD51__ echo.c -o echo -z max-page-size=4096 -Ttext=0 \
		 -L$(LIBC_DIR) -l$(LIBC) -pie -static -no-dynamic-linker -z max-page-size=4096 -Ttext=0 --no-export-dynamic 
